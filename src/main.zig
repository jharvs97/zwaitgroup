const std = @import("std");
const WaitGroup = @import("wait_group.zig").WaitGroup;

fn worker(wait_group: *WaitGroup, id: u32, wait_ms: u64) void {
    std.debug.print("Worker {} starting\n", .{id});
    std.time.sleep(std.time.ns_per_ms * wait_ms);
    std.debug.print("Worker {} finishing took {}ms\n", .{ id, wait_ms });
    wait_group.done();
}

pub fn main() !void {
    var wait_group = WaitGroup{};
    const worker_count = 10;

    var prng = std.rand.DefaultPrng.init(0);
    var random = prng.random();

    var i = @as(u32, 0);
    while (i < worker_count) : (i += 1) {
        wait_group.add(1);
        _ = try std.Thread.spawn(.{}, worker, .{ &wait_group, i, random.intRangeAtMost(u64, 100, 2000) });
    }

    wait_group.wait();
}
