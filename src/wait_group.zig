const std = @import("std");

pub const WaitGroup = struct {
    const Self = @This();
    const atomicOrdering = std.atomic.Ordering.Monotonic;

    count: std.atomic.Atomic(i32) = std.atomic.Atomic(i32).init(0),
    cond: std.Thread.Condition = .{},

    pub fn add(self: *Self, count: u32) void {
        self.addInternal(@intCast(i32, count));
    }

    pub fn done(self: *Self) void {
        self.addInternal(-1);
        self.cond.signal();
    }

    pub fn wait(self: *Self) void {
        var mutex = std.Thread.Mutex{};
        mutex.lock();
        defer mutex.unlock();

        while (self.count.load(atomicOrdering) != 0) {
            self.cond.wait(&mutex);
        }
    }

    fn addInternal(self: *Self, count: i32) void {
        _ = self.count.fetchAdd(count, atomicOrdering);
    }
};
